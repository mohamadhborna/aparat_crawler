FROM python:3.9.6-alpine

RUN apk add build-base postgresql-dev gcc python3-dev libpq musl-dev

WORKDIR  /app

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt